
import java.util.*;

//https://www.programmersought.com/article/98722661460/
//https://source-academy.github.io/sicp/chapters/2.3.4.html

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   private Node[] nodes = null;
   private Node treeRoot = null;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman(byte[] original) {
      if (original == null) {
         throw new RuntimeException("Empty input");
      }
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      int len = 0;
      for (Node node : nodes) {
         if (node != null) {
            len += node.bit * node.freq;
         }
      }
      return len;
   }

   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode(byte[] origData) {

      Node[] nodes = genFreqTable(origData);
      this.nodes = new Node[256];
      this.treeRoot = buildTree(nodes);

      if (this.treeRoot != null) {
         this.treeRoot.genMapTable("", 0, 0, this.nodes);
      }

      byte[] encodedBytes = new byte[this.calcOutBytes()];

      int curLen = 3;
      int curByte = 0;
      int pos = 0;
      Node node;

      for (byte b : origData) {
         node = this.nodes[b & 0xFF];
         if (node == null) {
            throw new RuntimeException("Error in frequency table mapping!");
         }
         int mask = node.mask;
         if (node.bit + curLen <= 8) {
            mask = mask << (8 - curLen - node.bit);
            curLen = curLen + node.bit;
            curByte = curByte | mask;
         } else {
            int over = -((8 - curLen) - node.bit);
            mask = mask >>> over;
            curByte = curByte | mask;
            encodedBytes[pos] = (byte) curByte;
            pos++;
            curByte = (node.mask << (8 - over)) & 0x00FF;
            curLen = over;
         }
      }

      int padLen = (8 - curLen);
      padLen = padLen << 5;

      encodedBytes[0] = (byte) (((int) encodedBytes[0] | padLen));

      if (curLen > 0) {
         encodedBytes[pos] = (byte) curByte;
      }
      return encodedBytes;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode(byte[] encodedData) {
      if (this.treeRoot == null) {
         throw new RuntimeException("Tree root not Found for the data, try to encode first");
      }

      if (encodedData.length == 0) { return null; }

      int curCodeLen = 0;
      int bitCombination = 0;

      int maxCodeLen = treeRoot.maxCodeLen(0);
      int bitLen = this.bitLength();
      int bitCount = 0;

      ArrayList<Byte> byteArrayList = new ArrayList<>();

      for (byte b : encodedData) {
         int b1 = b  & 0xFF;
         for (int j = 7; j >= 0; j--) {
            bitCount++;
            if ((bitCount > 3) && ((bitCount - 3) <= bitLen)) {
               boolean bit = ((b1 & (1 << j)) == (1 << j));
               if (curCodeLen < maxCodeLen) {
                  curCodeLen++;

                  bitCombination = bitCombination << 1;
                  if (bit)
                     bitCombination = bitCombination + 1;

                  int decodedByte = treeRoot.findNode(bitCombination);
                  if (decodedByte >= 0) {
                     byteArrayList.add((byte) decodedByte);
                     curCodeLen = 0;
                     bitCombination = 0;
                  }
               } else {
                  throw new RuntimeException("Code length exceeds maximum code length in Tree root");
               }
            }

         }
      }
      byte[] decodedBytes = new byte[byteArrayList.size()];

      for (int i = 0; i < byteArrayList.size(); i++) {
         decodedBytes[i] = byteArrayList.get(i);
      }
      return decodedBytes;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }

   private int findMin(Node[] input) {
      int count = Integer.MAX_VALUE;
      int node = -1;
      for (int i = 0; i < input.length; i++) {
         if (input[i] != null && input[i].freq < count) {
            count = input[i].freq;
            node = i;
         }
      }
      return node;
   }

   private Node[] genFreqTable(byte[] data) {
      int[] freq = new int[256];
      int count = 0;
      for (byte b : data) {
         if (freq[b & 0xFF] == 0)
            count++;
         freq[b & 0xFF]++;
      }

      Node[] nodes = new Node[count];
      count = 0;

      for (int i = 0; i < freq.length; i++) {
         if (freq[i] > 0) {
            Node node = new Node();
            node.data = (byte) i;
            node.freq = freq[i];
            nodes[count++] = node;
         }
      }

      return nodes;
   }

   private Node buildTree(Node[] nodes) {

      boolean finished = false;

      Node[] nodesClone = nodes.clone();

      Node left;
      Node right;
      Node parent;

      int min1;
      int min2;

      while (!finished) {
         min1 = findMin(nodesClone);
         if (min1 == -1) {
            finished = true;
            return null;
         } else {
            left = nodesClone[min1];
            nodesClone[min1] = null;
            min2 = findMin(nodesClone);
            if (min2 == -1) {
               finished = true;
               return left;
            } else {
               right = nodesClone[min2];
               nodesClone[min2] = null;
               parent = new Node();
               parent.freq = left.freq + right.freq;
               parent.left = left;
               parent.right = right;
               nodesClone[min1] = parent;
            }
         }
      }
      return null;
   }

   private int calcOutBytes() { return (int) Math.ceil((this.bitLength() + 3) / 8.0); }
}

class Node {
   int freq;
   byte data;
   int bit;
   int mask;
   Node right = null;
   Node left = null;

   boolean isNode() {
      return (right == null && left == null);
   }

   int index() {
      if (!this.isNode())
         return -1;
      return data & 0xFF;
   }

   @Override
   public String toString() { return "Node: data = " + data + ", frequency: " + freq; }

   void genMapTable(String node, int mask, int len, Node[] huffCodesTable) {
      if (this.isNode()) {
         if (len == 0)
            len = len + 1;
         this.bit = len;
         this.mask = mask;
         huffCodesTable[this.index()] = this;
      } else {
         if (this.left != null) {
            mask = mask << 1;
            this.left.genMapTable(node + "0", mask, len + 1, huffCodesTable);
         }
         if (this.right != null) {
            mask = mask + 1;

            this.right.genMapTable(node + "1", mask, len + 1, huffCodesTable);
         }
      }
   }

   public int findNode(int bitCombination) {

      int byteDataAsInt;

      if (this.isNode()) {
         if (this.mask == bitCombination) return this.data;
      }

      if (this.left != null) {
         byteDataAsInt = this.left.findNode(bitCombination);
         if (byteDataAsInt != -1) return byteDataAsInt;
      }
      if (this.right != null) {
         byteDataAsInt = this.right.findNode(bitCombination);
         return byteDataAsInt;
      }

      return -1;
   }

   public int maxCodeLen(int i) {
      if (this.isNode()) {
         return i + 1;
      }

      int left = i, right = i;
      if (this.left != null) {
         left = this.left.maxCodeLen(i + 1);
      }
      if (this.right != null) {
         right = this.right.maxCodeLen(i + 1);
      }
      return Math.max(left, right);
   }
}

